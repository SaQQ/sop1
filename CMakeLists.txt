cmake_minimum_required(VERSION 3.10)

project(sop1code)

add_subdirectory(01_hello_world)
add_subdirectory(02_filesystem2)
add_subdirectory(03_processes)
add_subdirectory(04_signals)
add_subdirectory(06_threads)
add_subdirectory(07_synchro)
#add_subdirectory(aio.old)
#add_subdirectory(sched.old)
